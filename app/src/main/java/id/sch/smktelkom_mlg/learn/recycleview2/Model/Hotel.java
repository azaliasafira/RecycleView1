package id.sch.smktelkom_mlg.learn.recycleview2.Model;

import android.graphics.drawable.Drawable;

/**
 * Created by azalia on 15/02/2018.
 */

public class Hotel {
    public String judul, deskripsi;
    public Drawable foto;

    public Hotel(String judul, String deskripsi, Drawable foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.foto = foto;
    }

}
